/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "aj-malloc2.h"

// this must be *after* the standard headers
#include "libc_emu.h"

void * aj_libcemu_malloc(size_t size) {
	if (size == 0) {
		return NULL;
	}

	void * buf = aj_alloc(size);

	if (buf == NULL) {
		errno = ENOMEM;
	}
	return buf;
}


void aj_libcemu_free(void *p) {
	aj_free(p);
}


void * aj_libcemu_calloc(size_t n, size_t size) {
	if (n == 0 || size == 0) {
		return NULL;
	}

	size_t total = n * size;

	if (total / size != n) {
		fprintf(stderr, "PANIC: aj_libcemu_calloc with huge size\n");
		abort();
	}

	void *p = aj_alloc(total);

	if (p == NULL) {
		errno = ENOMEM;
		return NULL;
	}

	memset(p, 0, total);

	return p;
}


void * aj_libcemu_realloc(void *p, size_t new_size) {
	if (p == NULL) {
		return aj_libcemu_malloc(new_size);
	}

	void *new_p = aj_realloc(p, new_size);

	if (new_p == NULL) {
		errno = ENOMEM;
	}
	return new_p;
}


char * aj_libcemu_strdup(const char *p) {
	size_t len = strlen(p);

	// note "len+1" to include the trailing NUL byte

	char *new_p = (char *)aj_alloc(len+1);

	if (new_p == NULL) {
		errno = ENOMEM;
		return NULL;
	}

	memcpy(new_p, p, len+1);

	return new_p;
}


char * aj_libcemu_strndup(const char *p, size_t n) {
	size_t len = strlen(p);
	if (len > n)
		len = n;

	// note "len+1" to include the trailing NUL byte

	char *new_p = (char *)aj_alloc(len+1);

	if (new_p == NULL) {
		errno = ENOMEM;
		return NULL;
	}

	if (len > 0) {
		memcpy(new_p, p, len);
	}

	new_p[len] = 0;

	return new_p;
}
