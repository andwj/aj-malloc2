/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#ifndef _AJ_MALLOC2_H_
#define _AJ_MALLOC2_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

void * aj_alloc   (size_t size);
void * aj_realloc (void *p, size_t size);
void   aj_free    (const void * p);

void   aj_malloc2_trim_caches (void);
void   aj_malloc2_print_stats (void);

#ifdef __cplusplus
}
#endif

#endif  /* _AJ_MALLOC2_H_ */
