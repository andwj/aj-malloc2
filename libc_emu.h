/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#ifndef _AJ_LIBCEMU_H_
#define _AJ_LIBCEMU_H_

/* NOTE: must include this file *after* the standard headers */

#undef  malloc
#define malloc(s)  aj_libcemu_malloc(s)
void * aj_libcemu_malloc(size_t size);

#undef  free
#define free(p)  aj_libcemu_free(p)
void   aj_libcemu_free(void *p);

#undef  calloc
#define calloc(n, s)  aj_libcemu_calloc(n, s)
void * aj_libcemu_calloc(size_t n, size_t size);

#undef  realloc
#define realloc(p, s)  aj_libcemu_realloc(p, s)
void * aj_libcemu_realloc(void *p, size_t size);

#undef  strdup
#define strdup(p)  aj_libcemu_strdup(p)
char * aj_libcemu_strdup(const char *p);

#undef  strndup
#define strndup(p, n)  aj_libcemu_strndup(p, n)
char * aj_libcemu_strndup(const char *p, size_t n);

#endif  /* _AJ_LIBCEMU_H_ */
