/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "m2_internal.h"


#define SMALL_CATEGORIES  6

#define SMALL_CHUNK_SIZE  4096

#define SMALL_MAX_BLOCKS  24


struct SmallChunk {
	// `next` and `prev` are links in a SmallChunk list.
	struct RawBlock  raw;

	// the category implies the object size (etc).
	uint32_t  category;
	uint32_t  free_obj;

	// a bitmap follows this header.
	// the bitmap size is a multiple of 8 bytes.
	// bits are packed into 64-bit words.
	// a "1" bit indicates free, a "0" bit indicates used.
	// unused portions are forced to zero.

	// objects follow the bitmap (with a possible gap).
};

#define SC_NEXT(chunk)  ((struct SmallChunk *) (chunk)->raw.next)
#define SC_PREV(chunk)  ((struct SmallChunk *) (chunk)->raw.prev)

#define SC_BITMAP(chunk)  ((uint64_t *) ((uint8_t *)(chunk) + sizeof(struct SmallChunk)))


struct SmallList {
	// this list has chunks with at least one free slot
	struct SmallChunk * first;
	struct SmallChunk * last;

	// this list has chunks which are full
	struct SmallChunk * dead_head;
	struct SmallChunk * dead_tail;

	uint32_t  obj_size;

	uint32_t  max_obj;      // pre-computed values
	uint32_t  bitmap_len;   //
	uint32_t  data_offset;  //
};


static struct {
	// there is one list for each size category
	struct SmallList  list[SMALL_CATEGORIES];

	// the underlying memory for the SmallChunk pages.
	//
	// each `RawBlock` is divided into multiple SmallChunks pages.
	// initially a raw block is fully free, chunks get allocated
	// when needed (i.e. unable to use any previous ones).
	// once these chunks run out, a new larger `RawBlock` is added.

	size_t   num_blocks;
	size_t   last_block_size;
	struct RawBlock  * blocks[SMALL_MAX_BLOCKS];

	size_t   chunk_used;   // in last RawBlock
	size_t   chunk_total;

	// damn lies and statistics
	size_t   obj_count;
	size_t   used_memory;
} Szone;


static int categorize(size_t size) {
	if (size <= 16) return 0;
	if (size <= 32) return 1;
	if (size <= 64) return 2;

	if (size <= 128)  return 3;
	if (size <= 192)  return 4;

	/* size <= 256 */ return 5;
}


static const uint32_t small_object_sizes[SMALL_CATEGORIES] = {
	16, 32, 64,  128, 192, 256
};


static void init_category(int cat) {
	// determine number of objects we can store in a page.
	//
	// it is slighly tricky since the size of the bitmap area
	// depends on the number of objects, but space for objects
	// depends on the size of the bitmap.

	uint32_t obj_size = small_object_sizes[cat];

	uint32_t n;
	uint32_t bitmap_len;

	for (n = SMALL_CHUNK_SIZE/8 ; n > 0 ; n--) {
		bitmap_len = ((n + 63) / 64);

		uint32_t total = sizeof(struct SmallChunk) + bitmap_len * 8 + n * obj_size;

		if (total <= SMALL_CHUNK_SIZE) {
			break;
		}
	}

	struct SmallList * list = &Szone.list[cat];

	// the offset calc here guarantees the alignment rules
	// (i.e. objects larger than 8 bytes have a 16-byte alignment).

	list->obj_size    = obj_size;
	list->max_obj     = n;
	list->bitmap_len  = bitmap_len;
	list->data_offset = SMALL_CHUNK_SIZE - n * obj_size;
}


static struct RawBlock * new_small_block(void) {
	int n = Szone.num_blocks;
	if (n >= SMALL_MAX_BLOCKS) {
		return NULL;
	}

	size_t size = Szone.last_block_size;

	// raw blocks get progressively larger...
	if (size == 0) {
		size = SMALL_CHUNK_SIZE;
	} else if (size < MAX_OS_ALLOC) {
		size = size * 2;
	}

	struct RawBlock * blk = ajm2_get_pages(size, SMALL_ID);
	if (blk == NULL) {
		return NULL;
	}

	ajm2_range_insert(blk);

	Szone.blocks[n]  = blk;
	Szone.num_blocks = n + 1;
	Szone.last_block_size = size;

	Szone.chunk_used  = 0;
	Szone.chunk_total = size / SMALL_CHUNK_SIZE;

	// update stats
	Szone.used_memory += blk->size;

	return blk;
}


static void init_bitmap(struct SmallChunk * chunk, struct SmallList * list) {
	uint64_t * bitmap = SC_BITMAP(chunk);

	if (list->bitmap_len >= 2) {
		size_t len = (list->bitmap_len - 1) * 8;
		memset(bitmap, 0xFF, len);
	}

	// the last word is tricky, the upper bits MUST be zero.
	// I prefer readable code here over inscrutable cleverness.
	uint64_t last_word = 0;

	unsigned int count = list->max_obj - (list->bitmap_len - 1) * 64;

	for (; count > 0 ; count--) {
		last_word = (last_word << 1) | 1;
	}

	bitmap[list->bitmap_len - 1] = last_word;
}


static struct SmallChunk * new_small_chunk(int cat) {
	if (Szone.chunk_used >= Szone.chunk_total) {
		if (new_small_block() == NULL) {
			return NULL;
		}
	}

	struct SmallList * list = &Szone.list[cat];

	// slice off a new chunk from the raw block
	size_t index = Szone.chunk_used;
	Szone.chunk_used += 1;

	uint8_t * base = (uint8_t *) Szone.blocks[Szone.num_blocks - 1];

	struct SmallChunk * chunk = (struct SmallChunk *) (base + index * (size_t)SMALL_CHUNK_SIZE);

	// init the chunk
	chunk->raw.ident = SMALL_ID;

	chunk->category = cat;
	chunk->free_obj = list->max_obj;

	init_bitmap(chunk, list);

	// add it to the head of the list
	chunk->raw.next = list->first;
	chunk->raw.prev = NULL;

	if (list->first != NULL) {
		list->first->raw.prev = chunk;
	} else {
		list->last = chunk;
	}
	list->first = chunk;

	return chunk;
}


static void kill_small_chunk(struct SmallList * list, struct SmallChunk * chunk) {
	// remove from alive list
	if (SC_PREV(chunk) != NULL) {
		SC_PREV(chunk)->raw.next = SC_NEXT(chunk);
	} else {
		list->first = SC_NEXT(chunk);
	}

	if (SC_NEXT(chunk) != NULL) {
		SC_NEXT(chunk)->raw.prev = SC_PREV(chunk);
	} else {
		list->last = SC_PREV(chunk);
	}

	// add to end of dead list
	chunk->raw.next = NULL;
	chunk->raw.prev = list->dead_tail;

	if (list->dead_tail != NULL) {
		list->dead_tail->raw.next = chunk;
	} else {
		list->dead_head = chunk;
	}
	list->dead_tail = chunk;
}


static void resurrect_small_chunk(struct SmallList * list, struct SmallChunk * chunk) {
	// remove from dead list
	if (SC_PREV(chunk) != NULL) {
		SC_PREV(chunk)->raw.next = SC_NEXT(chunk);
	} else {
		list->dead_head = SC_NEXT(chunk);
	}

	if (SC_NEXT(chunk) != NULL) {
		SC_NEXT(chunk)->raw.prev = SC_PREV(chunk);
	} else {
		list->dead_tail = SC_PREV(chunk);
	}

	// add to end of alive list
	chunk->raw.next = NULL;
	chunk->raw.prev = list->last;

	if (list->last != NULL) {
		list->last->raw.next = chunk;
	} else {
		list->first = chunk;
	}
	list->last = chunk;
}


static unsigned int grab_free_slot(struct SmallChunk * chunk) {
	uint64_t * bitmap = SC_BITMAP(chunk);

	unsigned int slot  = 0;
	unsigned int count = Szone.list[chunk->category].bitmap_len;

	for (; count > 0 ; bitmap++, slot += 64, count--) {
		uint64_t bits = *bitmap;
		uint64_t mask = 1;

		if (bits != 0) {
			for (;;) {
				if ((bits & mask) != 0) {
					// found it!
					*bitmap = bits ^ mask;
					return slot;
				}
				mask = mask << 1;
				slot += 1;
			}
		}
	}

	ajm2_panic("bad bitmap in SmallChunk (memory corruption)");
	return 0;  /* NOT REACHED */
}


static struct SmallChunk * chunk_for_pointer(void * p, struct RawBlock * blk) {
	size_t ofs = (uint8_t *)p - (uint8_t *)blk;

	// clear the lower bits of offset
	ofs = ofs & ~(size_t)(SMALL_CHUNK_SIZE - 1);

	struct SmallChunk * chunk = (struct SmallChunk *) ((uint8_t *)blk + ofs);

	if (chunk->raw.ident != SMALL_ID) {
		ajm2_panic("bad chunk->ident (memory corruption)");
	}
	return chunk;
}


static unsigned int slot_within_chunk(void * p, struct SmallChunk * chunk, struct SmallList * list) {
	size_t ofs = (uint8_t *)p - (uint8_t *)chunk;

	if (ofs >= list->data_offset) {
		ofs = ofs - list->data_offset;

		unsigned int slot = ofs / list->obj_size;

		if (slot < list->max_obj && (slot * list->obj_size) == ofs) {
			return slot;
		}
	}

	ajm2_panic("free/realloc with bad pointer");
	return 0;  /* NOT REACHED */
}


void * ajm2_small_alloc(size_t size) {
	int cat = categorize(size);

	struct SmallList * list = &Szone.list[cat];

	if (list->obj_size == 0) {
		init_category(cat);
	}

	if (list->first == NULL) {
		if (new_small_chunk(cat) == NULL) {
			return NULL;
		}
	}

	// this should never be NULL
	struct SmallChunk * chunk = list->first;

	// this also clears the bitmap bit
	unsigned int slot = grab_free_slot(chunk);

	// move full chunks to the dead list
	chunk->free_obj -= 1;
	if (chunk->free_obj == 0) {
		kill_small_chunk(list, chunk);
	}

	// update stats
	Szone.obj_count += 1;

	uint8_t * base = (uint8_t *)chunk + list->data_offset;

	return base + slot * list->obj_size;
}


void * ajm2_small_realloc(void * p, size_t new_size, struct RawBlock * blk) {
	struct SmallChunk * chunk = chunk_for_pointer(p, blk);
	struct SmallList  * list  = &Szone.list[chunk->category];

	unsigned int slot = slot_within_chunk(p, chunk, list);

	if (new_size > list->obj_size) {
		return ajm2_dumb_realloc(p, new_size, list->obj_size);
	}

	// check for double free
	size_t   bm_index = (slot / 64);
	uint64_t bm_bit   = (uint64_t)1 << (uint64_t)(slot & 63);

	uint64_t * bitmap = SC_BITMAP(chunk);

	if (bitmap[bm_index] & bm_bit) {
		ajm2_panic("realloc of a free object");
	}

	// can stay in current spot
	return p;
}


void ajm2_small_free(void * p, struct RawBlock * blk) {
	struct SmallChunk * chunk = chunk_for_pointer(p, blk);
	struct SmallList  * list  = &Szone.list[chunk->category];

	unsigned int slot = slot_within_chunk(p, chunk, list);

	// check for double free
	size_t   bm_index = (slot / 64);
	uint64_t bm_bit   = (uint64_t)1 << (uint64_t)(slot & 63);

	uint64_t * bitmap = SC_BITMAP(chunk);

	if (bitmap[bm_index] & bm_bit) {
		ajm2_panic("double free detected");
	}

	// set the bitmap bit
	bitmap[bm_index] |= bm_bit;

	// move empty chunks to the alive list
	if (chunk->free_obj == 0) {
		resurrect_small_chunk(list, chunk);
	}
	chunk->free_obj += 1;

	// update stats
	Szone.obj_count -= 1;
}


void ajm2_print_small_stats(void) {
	printf("Small objects: %d totalling %dK\n",
		(int)Szone.obj_count,
		(int)(Szone.used_memory / 1024U));
}
