/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#else
// Linux, *BSD, MacOS
#include <unistd.h>
#include <sys/mman.h>
#endif

#include "m2_internal.h"


static size_t  os_page_size;
static size_t  os_used_pages;


static void query_page_size(void) {
#ifdef _WIN32
	SYSTEM_INFO si;
	GetSystemInfo(&si);

	os_page_size = (size_t)si.dwPageSize;
#else
	// Linux, *BSD, MacOS
	long size = sysconf(_SC_PAGESIZE);

	if (size < 0) {
		ajm2_panic("could not determine page size");
	}

	os_page_size = (size_t)size;
#endif

	if (os_page_size < 4096) {
		ajm2_panic("page size is too small (< 4096 bytes)");
	}
	if (os_page_size > 16384) {
		ajm2_panic("page size is too large (> 16384 bytes)");
	}
}


struct RawBlock * ajm2_get_pages(size_t size, size_t ident) {
	if (size == 0) {
		ajm2_panic("ajm2_get_pages with size == 0");
	}
	if (size > MAX_OS_ALLOC) {
		ajm2_panic("ajm2_get_pages with size > 2GB");
	}

	if (os_page_size == 0) {
		query_page_size();
	}

	// round up to the hardware page size
	size_t pages = (size + os_page_size - 1) / os_page_size;

	size = pages * os_page_size;

#ifdef _WIN32
	void *p = VirtualAlloc(NULL, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);

	if (p == NULL) {
		return NULL;
	}
#else
	// Linux, *BSD, MacOS
	void *p = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1 /* fd */, 0 /* offset */);

	if (p == MAP_FAILED) {
		return NULL;
	}
#endif

	struct RawBlock * blk = (struct RawBlock *)p;

	blk->ident = ident;
	blk->size  = size;

	// update stats
	os_used_pages += pages;

	return blk;
}


void ajm2_release_pages(struct RawBlock * blk) {
	if (blk == NULL) {
		ajm2_panic("ajm2_release_pages with blk == NULL");
	}
	if (blk->size == 0) {
		ajm2_panic("ajm2_release_pages with blk->size == 0");
	}
	if (blk->ident == 0) {
		ajm2_panic("ajm2_release_pages with blk->ident == 0");
	}

	blk->ident = 0;

	if (os_page_size == 0) {
		query_page_size();
	}

	size_t pages = blk->size / os_page_size;

#ifdef _WIN32
	// give size as zero, as per MSDN documentation
	if (0 == VirtualFree((void *)blk, 0, MEM_RELEASE)) {
		ajm2_warning("VirtualFree failed in ajm2_release_pages");
		return;
	}
#else
	// Linux, *BSD, MacOS
	if (0 != munmap((void *)blk, blk->size)) {
		ajm2_warning("munmap failed in ajm2_release_pages");
		return;
	}
#endif

	// update stats
	os_used_pages -= pages;
}


void ajm2_print_page_stats(void) {
	if (os_page_size == 0) {
		query_page_size();
	}
	printf("Page size: %d bytes\n", (int)os_page_size);
	printf("Pages in use: %d\n", (int)os_used_pages);
}
