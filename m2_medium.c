/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>

#include "m2_internal.h"


#define M_CHUNK_FREE  0xD540F2EE
#define M_CHUNK_USED  0xD541C5ED

#define M_NONE  0

struct MediumChunk {
	uint32_t  ident;  // either M_CHUNK_FREE or M_CHUNK_USED
	uint32_t  size;   // including this header and unused portion at end

	uint32_t  next;   // offset of next chunk, M_NONE for last
	uint32_t  prev;   // offset of previous chunk, M_NONE for first

	// free space or allocated data follows this header
};


#define MEDIUM_MAX_BLOCKS  24
#define MEDIUM_START_SIZE  4096

#define MB_CHUNK(blk, ofs)  ((struct MediumChunk *) ((uint8_t *)(blk) + (ofs)))

struct MediumBlock {
	struct RawBlock  raw;

	// linked list of chunks, offsets from base of this MediumBlock.
	uint32_t  first;
	uint32_t  last;

	// current position for allocations in this block.
	uint32_t  rover;
	uint32_t  _pad;

	// following this header are all the chunks...
};


static struct {
	// the list of MediumBlocks (which are also a RawBlock).
	size_t   num_blocks;
	size_t   last_block_size;
	struct MediumBlock * blocks[MEDIUM_MAX_BLOCKS];

	// current block being used for allocations.
	size_t   rover;

	// statistics
	size_t   obj_count;
	size_t   used_memory;
} Mzone;


static struct MediumBlock * new_medium_block(void) {
	int n = Mzone.num_blocks;
	if (n >= MEDIUM_MAX_BLOCKS) {
		return NULL;
	}

	size_t size = Mzone.last_block_size;

	// raw blocks get progressively larger...
	if (size == 0) {
		size = MEDIUM_START_SIZE;
	} else if (size < MAX_OS_ALLOC) {
		size = size * 2;
	}

	struct RawBlock * raw = ajm2_get_pages(size, MEDIUM_ID);
	if (raw == NULL) {
		return NULL;
	}

	ajm2_range_insert(raw);

	struct MediumBlock * blk = (struct MediumBlock *) raw;

	Mzone.blocks[n]  = blk;
	Mzone.num_blocks = n + 1;
	Mzone.last_block_size = size;

	Mzone.rover = n;

	// create a MediumChunk which fills the block
	uint32_t offset = sizeof(struct MediumBlock);

	// round up to a multiple of 16
	offset = ((offset + 15U) >> 4) << 4;

	struct MediumChunk * chunk = MB_CHUNK(blk, offset);

	chunk->ident = M_CHUNK_FREE;
	chunk->size  = raw->size - offset;
	chunk->next  = M_NONE;
	chunk->prev  = M_NONE;

	// initialize the MediumBlock
	blk->first = offset;
	blk->last  = offset;
	blk->rover = offset;

	// update stats
	Mzone.used_memory += raw->size;

	return blk;
}


static void try_create_fragment(uint32_t offset, uint32_t size, struct MediumBlock * blk) {
	struct MediumChunk * chunk = MB_CHUNK(blk, offset);

	uint32_t extra = chunk->size - size;

	if (extra >= SMALL_OBJ_SIZE*2) {
		uint32_t new_offset = offset + size;

		// round up to a multiple of 16
		new_offset = ((new_offset + 15U) >> 4) << 4;

		struct MediumChunk * new_chunk = MB_CHUNK(blk, new_offset);
		struct MediumChunk * next;

		new_chunk->ident = M_CHUNK_FREE;
		new_chunk->size  = chunk->size - (new_offset - offset);

		// link into list
		new_chunk->next  = chunk->next;
		new_chunk->prev  = offset;

		if (chunk->next == M_NONE) {
			blk->last = new_offset;
		} else {
			next = MB_CHUNK(blk, chunk->next);
			next->prev = new_offset;
		}

		chunk->next = new_offset;
		chunk->size = new_offset - offset;
	}
}


static void try_merge_next_chunk(uint32_t offset, struct MediumBlock * blk) {
	struct MediumChunk * chunk = MB_CHUNK(blk, offset);

	if (chunk->ident != M_CHUNK_FREE) {
		return;
	}
	if (chunk->next == M_NONE) {
		return;
	}

	struct MediumChunk * next = MB_CHUNK(blk, chunk->next);
	struct MediumChunk * next2;

	if (next->ident != M_CHUNK_FREE) {
		return;
	}

	// perform the merge...

	if (blk->rover == chunk->next) {
		blk->rover = offset;
	}

	if (next->next == M_NONE) {
		blk->last = offset;
	} else {
		next2 = MB_CHUNK(blk, next->next);
		next2->prev = offset;
	}

	chunk->next =  next->next;
	chunk->size += next->size;

	next->ident = (uint32_t) -1;
}


static void * alloc_in_medium_block(size_t size, struct MediumBlock * blk) {
	// account for header
	size += sizeof(struct MediumChunk);

	uint32_t start = blk->rover;

	uint32_t offset = 0;
	struct MediumChunk * chunk = NULL;

	for (;;) {
		offset = blk->rover;
		chunk  = MB_CHUNK(blk, offset);

		blk->rover = chunk->next;
		if (blk->rover == M_NONE) {
			blk->rover = blk->first;
		}

		if (chunk->ident == M_CHUNK_FREE) {
			if (chunk->size >= size) {
				break;
			}
		} else if (chunk->ident == M_CHUNK_USED) {
			// skip it
		} else {
			ajm2_panic("memory corruption detected");
		}

		if (blk->rover == start) {
			// we visited every chunk without luck
			return NULL;
		}
	}

	if ((chunk->size & 15) != 0) {
		ajm2_panic("memory corruption detected");
	}

	// found a usable chunk.
	// see if the extra space should become a new free chunk.
	try_create_fragment(offset, size, blk);

	chunk->ident = M_CHUNK_USED;

	Mzone.obj_count += 1;

	blk->rover = chunk->next;
	if (blk->rover == M_NONE) {
		blk->rover = blk->first;
	}

	return (uint8_t *)chunk + sizeof(struct MediumChunk);
}


void * ajm2_medium_alloc(size_t size) {
	if (Mzone.num_blocks == 0) {
		new_medium_block();
	}

	// try all existing blocks...
	uint32_t start = Mzone.rover;

	for (;;) {
		struct MediumBlock * blk = Mzone.blocks[Mzone.rover];

		void * p = alloc_in_medium_block(size, blk);
		if (p != NULL) {
			// okay!
			return p;
		}

		Mzone.rover += 1;
		if (Mzone.rover >= Mzone.num_blocks) {
			Mzone.rover = 0;
		}

		if (Mzone.rover == start) {
			break;
		}
	}

	// no existing block could satisfy the request.
	// the reason might be they were all simply too small, so we need
	// to loop here to ensure we get a large enough block.

	for (;;) {
		struct MediumBlock * blk = new_medium_block();
		if (blk == NULL) {
			return NULL;
		}

		void * p = alloc_in_medium_block(size, blk);
		if (p != NULL) {
			return p;
		}
	}
}


void * ajm2_medium_realloc(void * p, size_t new_size, struct RawBlock * raw) {
	struct MediumBlock * blk = (struct MediumBlock *)raw;

	size_t offset = (uint8_t *)p - (uint8_t *)blk;
	size_t min_offset = sizeof(struct MediumBlock) + sizeof(struct MediumChunk);

	if (offset < min_offset || (offset & 7) != 0) {
		ajm2_panic("realloc with bad pointer");
	}

	offset -= sizeof(struct MediumChunk);

	struct MediumChunk * chunk = MB_CHUNK(blk, offset);

	if (chunk->ident == M_CHUNK_FREE) {
		ajm2_panic("realloc of a free object");
	} else if (chunk->ident != M_CHUNK_USED) {
		ajm2_panic("realloc with bad pointer");
	} else if ((chunk->size & 15) != 0 || (chunk->prev & 15) != 0) {
		ajm2_panic("memory corruption detected");
	}

	// can use same chunk if the new size fits
	if (new_size + sizeof(struct MediumChunk) <= chunk->size) {
		return p;
	}

	// when next block is free, try to expand into it
	if (chunk->next != M_NONE) {
		struct MediumChunk * next = MB_CHUNK(blk, chunk->next);

		uint32_t total = chunk->size + next->size;

		if (next->ident == M_CHUNK_FREE && new_size + sizeof(struct MediumChunk) <= total) {
			try_merge_next_chunk(offset, blk);
			try_create_fragment(offset, new_size, blk);

			return p;
		}
	}

	return ajm2_dumb_realloc(p, new_size, chunk->size);
}


void ajm2_medium_free(void * p, struct RawBlock * raw) {
	struct MediumBlock * blk = (struct MediumBlock *)raw;

	size_t offset = (uint8_t *)p - (uint8_t *)blk;
	size_t min_offset = sizeof(struct MediumBlock) + sizeof(struct MediumChunk);

	if (offset < min_offset || (offset & 7) != 0) {
		ajm2_panic("free with bad pointer");
	}

	offset -= sizeof(struct MediumChunk);

	struct MediumChunk * chunk = MB_CHUNK(blk, offset);

	if (chunk->ident == M_CHUNK_FREE) {
		ajm2_panic("double free detected");
	} else if (chunk->ident != M_CHUNK_USED) {
		ajm2_panic("free with bad pointer");
	} else if ((chunk->size & 15) != 0 || (chunk->prev & 15) != 0) {
		ajm2_panic("memory corruption detected");
	}

	// mark as free
	chunk->ident = M_CHUNK_FREE;

	Mzone.obj_count -= 1;

	// try to merge with neighboring free chunks
	int loop;

	for (loop = 0 ; loop < 2 ; loop++) {
		chunk = MB_CHUNK(blk, offset);

		if (chunk->next != M_NONE) {
			try_merge_next_chunk(offset, blk);
		}

		offset = chunk->prev;
		if (offset == 0) {
			break;
		}
	}
}


void ajm2_print_medium_stats(void) {
	printf("Medium objects: %d totalling %dK\n",
		(int)Mzone.obj_count,
		(int)(Mzone.used_memory / 1024U));
}
