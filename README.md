
AJ-MALLOC2
==========

by Andrew Apted, 2021.


About
-----

This is a low level memory allocator implementing the same facilities
as the C standard library functions `malloc`, `realloc` and `free`.
It aims to be good, easy-to-understand code while still providing
reasonable performance and memory usage.

The [ALLOCATOR.md](ALLOCATOR.md) document describes the design of this
memory allocator.

This code is portable, supporting both Windows OS and Unix-style OSes
like Linux and the BSDs.  Since MacOS is based on BSD Unix, it should
work there too but that has not been tested yet.

NOTE: this code is *not* thread-safe.  If you want to use this allocator
in a threaded program, then you will need to create wrapper functions
which enforce single threading, e.g. by locking and unlocking a mutex.


Legalese
--------

aj-malloc2 is under a permissive MIT license.

aj-malloc2 comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) document for the full terms.

