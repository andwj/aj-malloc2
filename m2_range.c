/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>

#include "m2_internal.h"


#define MAX_RANGES  128

static struct RawBlock * ranges[MAX_RANGES];
static int num_ranges;


struct RawBlock * ajm2_range_find(void * p) {
	// this is a binary search algorithm.

	int first = 0;
	int num   = num_ranges;

	while (num > 2) {
		int step = num / 2;

		struct RawBlock * blk = ranges[first + step];

		if (p < (void *)blk) {
			num = step;
		} else {
			first += step;
			num   -= step;
		}
	}

	for (; num > 0 ; first++, num--) {
		struct RawBlock * blk = ranges[first];

		if (p >= (void *)blk) {
			void * blk_end = (uint8_t *)blk + blk->size;

			if (p < blk_end) {
				return blk;
			}
		}
	}

	// not found
	return NULL;
}


void ajm2_range_insert(struct RawBlock * blk) {
	// this is a simple insertion sort.

	if (num_ranges >= MAX_RANGES) {
		ajm2_panic("overflow of the sorted RawBlock list");
	}

	int before;
	for (before = 0 ; before < num_ranges ; before++) {
		if ((void *)blk < (void *)ranges[before]) {
			break;
		}
	}

	// shift everything after the wanted spot up.
	int i;
	for (i = num_ranges ; i > before ; i--) {
		ranges[i] = ranges[i-1];
	}

	ranges[before] = blk;

	num_ranges += 1;
}


void ajm2_range_remove(struct RawBlock * blk) {
	int pos;
	for (pos = 0 ; pos < num_ranges ; pos++) {
		if (blk == ranges[pos]) {
			break;
		}
	}

	if (pos >= num_ranges) {
		ajm2_panic("RawBlock not found in the sorted list");
	}

	// shift everything down
	int i;
	for (i = pos ; i+1 < num_ranges ; i++) {
		ranges[i] = ranges[i+1];
	}

	num_ranges -= 1;

	// keep the remainder of the list as NULLs, which can help
	// debugging (you won't see any out-of-date pointers).
	ranges[num_ranges] = NULL;
}
