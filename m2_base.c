/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "aj-malloc2.h"
#include "m2_internal.h"


static struct RawBlock * block_for_ptr(void *p) {
	struct RawBlock * blk = ajm2_range_find(p);

	if (blk != NULL) {
		return blk;
	}

	blk = ajm2_large_find(p);
	return blk;
}


void * aj_alloc(size_t size) {
	if (size == 0) {
		return NULL;
	}

	if (size <= SMALL_OBJ_SIZE) {
		return ajm2_small_alloc(size);
	}

	if (size < LARGE_OBJ_SIZE) {
		return ajm2_medium_alloc(size);
	}

	return ajm2_large_alloc(size);
}


void * aj_realloc (void *p, size_t new_size) {
	if (p == NULL) {
		return aj_alloc(new_size);
	}

	if (new_size == 0) {
		aj_free(p);
		return NULL;
	}

	struct RawBlock * blk = block_for_ptr(p);
	if (blk == NULL) {
		ajm2_panic("realloc of invalid pointer");
	}

	if (blk->ident == SMALL_ID) {
		return ajm2_small_realloc(p, new_size, blk);

	} else if (blk->ident == MEDIUM_ID) {
		return ajm2_medium_realloc(p, new_size, blk);

	} else if (blk->ident == LARGE_ID) {
		return ajm2_large_realloc(p, new_size, blk);
	}

	ajm2_panic("bad blk->ident (memory corruption)");
	return NULL;  /* not reached */
}


void aj_free(const void * p0) {
	void * p = (void *)p0;

	if (p == NULL) {
		return;
	}

	struct RawBlock * blk = block_for_ptr(p);
	if (blk == NULL) {
		ajm2_panic("free of invalid pointer");
	}

	if (blk->ident == SMALL_ID) {
		ajm2_small_free(p, blk);

	} else if (blk->ident == MEDIUM_ID) {
		ajm2_medium_free(p, blk);

	} else if (blk->ident == LARGE_ID) {
		ajm2_large_free(blk);

	} else {
		ajm2_panic("bad blk->ident (memory corruption)");
	}
}


void aj_malloc2_trim_caches(void) {
	// nothing to trim....
}


void aj_malloc2_print_stats(void) {
	printf("\n");
	printf("aj-malloc2 statistics\n");
	printf("~~~~~~~~~~~~~~~~~~~~~\n");

	/* page stats */

	ajm2_print_page_stats();
	ajm2_print_large_stats();
	ajm2_print_medium_stats();
	ajm2_print_small_stats();

	fflush(stdout);
}


/* internal utilities */

void ajm2_panic(const char * msg) {
	fflush(stdout);
	fflush(stderr);

	fprintf(stderr, "\nAJ-MALLOC2 PANIC: %s\n", msg);
	fflush(stderr);

	abort();
}


void ajm2_warning(const char * msg) {
#if 0
	fflush(stdout);
	fflush(stderr);

	fprintf(stderr, "\nAJ-MALLOC2 WARNING: %s\n", msg);
	fflush(stderr);
#endif
}


void * ajm2_dumb_realloc(void * p, size_t new_size, size_t old_size) {
	void * new_p = aj_alloc(new_size);
	if (new_p == NULL) {
		return NULL;
	}

	memcpy(new_p, p, old_size);
	aj_free(p);

	return new_p;
}
