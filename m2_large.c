/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdlib.h>
#include <stdio.h>

#include "m2_internal.h"


#define LC_NEXT(blk)  ((struct RawBlock *) (blk)->next)
#define LC_PREV(blk)  ((struct RawBlock *) (blk)->prev)


static struct {
	struct RawBlock * first;
	struct RawBlock * last;

	size_t   obj_count;
	size_t   used_memory;
} Lzone;


void * ajm2_large_alloc(size_t size) {
	// account for header
	size += sizeof(struct RawBlock);

	struct RawBlock * blk = ajm2_get_pages(size, LARGE_ID);
	if (blk == NULL) {
		return NULL;
	}

	// add to head of list, to make large_find() slightly faster
	// for the most recent allocations.

	blk->next = Lzone.first;
	blk->prev = NULL;

	if (Lzone.first != NULL) {
		Lzone.first->prev = blk;
	} else {
		Lzone.last = blk;
	}
	Lzone.first = blk;

	// update stats
	Lzone.obj_count   += 1;
	Lzone.used_memory += blk->size;

	return (uint8_t *)blk + sizeof(struct RawBlock);
}


void * ajm2_large_realloc(void * p, size_t new_size, struct RawBlock * blk) {
	// can use same block if the new size fits
	if (new_size + sizeof(struct RawBlock) <= blk->size) {
		return p;
	}

	return ajm2_dumb_realloc(p, new_size, blk->size);
}


void ajm2_large_free(struct RawBlock * blk) {
	// remove from the list
	if (LC_PREV(blk) != NULL) {
		LC_PREV(blk)->next = LC_NEXT(blk);
	} else {
		Lzone.first = LC_NEXT(blk);
	}

	if (LC_NEXT(blk) != NULL) {
		LC_NEXT(blk)->prev = LC_PREV(blk);
	} else {
		Lzone.last = LC_PREV(blk);
	}

	// update stats
	Lzone.obj_count   -= 1;
	Lzone.used_memory -= blk->size;

	ajm2_release_pages(blk);
}


struct RawBlock * ajm2_large_find(void * p) {
	struct RawBlock * blk;

	for (blk = Lzone.first ; blk != NULL ; blk = LC_NEXT(blk)) {
		if (p >= (void *)blk) {
			void * blk_end = (uint8_t *)blk + blk->size;

			if (p < blk_end) {
				void * start = (uint8_t *)blk + sizeof(struct RawBlock);

				if (p != start) {
					ajm2_panic("free/realloc with bad pointer");
				}
				return blk;
			}
		}
	}

	// not found
	return NULL;
}


void ajm2_print_large_stats(void) {
	printf("Large objects: %d totalling %dK\n",
		(int)Lzone.obj_count,
		(int)(Lzone.used_memory / 1024U));
}
