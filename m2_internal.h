/* Copyright 2021 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stddef.h>
#include <stdint.h>

/* common defs */

#define SMALL_OBJ_SIZE   256
#define LARGE_OBJ_SIZE   (65536 - 64)

#define SMALL_ID     0xAA053477
#define MEDIUM_ID    0xBB3ED1C3
#define LARGE_ID     0xDD07426E

#define USED_CHUNK   0xC5ED03E3
#define FREE_CHUNK   0xF2EE0C47

#define MAX_OS_ALLOC  (2 * 1024 * 1024 * 1024U)


/* OS interface */

// a RawBlock is allocated directly from the operating system.
struct RawBlock {
	size_t  ident;
	size_t  size;

	void * next;  // a RawBlock or SmallChunk
	void * prev;  //
};


// allocate memory from the OS.
// the size will be rounded up to the page size.
// the result pointer is always at the beginning of a page.
// the memory can be read and written, but cannot execute code.
// returns NULL if the allocation fails.
struct RawBlock * ajm2_get_pages(size_t size, size_t ident);

// free memory back to the OS.
void ajm2_release_pages(struct RawBlock * blk);


/* large allocator */

// use the large allocator to allocate some memory.
// returns NULL if the allocation fails.
void * ajm2_large_alloc(size_t size);

// reallocate some memory in the large allocator.
// the `blk` pointer must be obtained via ajm2_large_find().
// returns NULL on failure.
void * ajm2_large_realloc(void * p, size_t new_size, struct RawBlock * blk);

// free a previously allocated large block.
// the `blk` pointer must be obtained via ajm2_large_find().
void ajm2_large_free(struct RawBlock * blk);

// find the header for memory previously allocated by large_alloc().
// returns NULL if no such memory is found.
struct RawBlock * ajm2_large_find(void * p);


/* medium allocator */

// use the medium allocator to allocate some memory.
// returns NULL if the allocation fails.
void * ajm2_medium_alloc(size_t size);

// reallocate some memory in the medium allocator.
// the `blk` pointer must be obtained via ajm2_range_find().
// returns NULL on failure.
void * ajm2_medium_realloc(void * p, size_t new_size, struct RawBlock * blk);

// free a previously allocated medium block.
// the `blk` pointer must be obtained via ajm2_range_find().
void ajm2_medium_free(void * p, struct RawBlock * blk);



/* small allocator */

// use the small allocator to allocate some memory.
// returns NULL if the allocation fails.
void * ajm2_small_alloc(size_t size);

// reallocate some memory in the small allocator.
// the `blk` pointer must be obtained via ajm2_range_find().
// returns NULL on failure.
void * ajm2_small_realloc(void * p, size_t new_size, struct RawBlock * blk);

// free a previously allocated small object.
// the `blk` pointer must be obtained via ajm2_range_find().
void ajm2_small_free(void * p, struct RawBlock * blk);


/* sorted ranges */

// find the block in the sorted list which contains the given pointer.
// returns NULL if no such block is found.
struct RawBlock * ajm2_range_find(void * p);

// insert a RawBlock into the sorted list.
// can be used by either the small or medium allocators.
// the information in `blk` is not modified.
void ajm2_range_insert(struct RawBlock * blk);

// remove a RawBlock from the sorted list.
// can be used by either the small or medium allocators.
// the information in `blk` is not modified.
void ajm2_range_remove(struct RawBlock * blk);


/* statistics */

void ajm2_print_page_stats(void);
void ajm2_print_large_stats(void);
void ajm2_print_medium_stats(void);
void ajm2_print_small_stats(void);


/* utilities */

void ajm2_panic  (const char * msg);
void ajm2_warning(const char * msg);

// perform a reallocation by allocating a new block, copying the data,
// and freeing the old block.
void * ajm2_dumb_realloc(void * p, size_t new_size, size_t old_size);

