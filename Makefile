#
#  AJ-MALLOC2
#
#  Makefile for Linux and BSD systems.
#

LIBRARY=libajmalloc2.a

CFLAGS=-Wall -Wextra -Wno-unused-parameter -g3 -O0

OBJS=m2_base.o m2_os.o m2_range.o \
	m2_large.o m2_medium.o m2_small.o \
	libc_emu.o

all: $(LIBRARY)
# test1 test2

clean:
	rm -f $(LIBRARY) *.o ERRS test1 test2

$(LIBRARY): $(OBJS)
	ar rcv $@ $^
	ranlib $@

test1: test1.c $(LIBRARY)
	$(CC) $(CFLAGS) $^ -o $@ ./$(LIBRARY)

test2: test2.c $(LIBRARY)
	$(CC) $(CFLAGS) $^ -o $@ ./$(LIBRARY)

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
