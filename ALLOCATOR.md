
MEMORY ALLOCATOR
================

This document describes the design of a general-purpose (non-GC)
memory allocator.

Internally there are three different allocators:
-  anything >= LARGE_OBJ_SIZE uses the large object allocator.
-  anything <= SMALL_OBJ_SIZE uses the small object allocator.
-  everything else uses the medium object allocator.

Currently LARGE_OBJ_SIZE is 64K and SMALL_OBJ_SIZE is 256 bytes.

Returned memory pointers from an allocation (or re-allocation) are
guaranteed to be aligned to 16 bytes.


LARGE OBJECT ALLOCATOR
----------------------

This allocates and frees the memory via the operating system.
On Linux and other Unices, it uses the `mmap` and `munmap` syscalls.
On Windows it uses `VirtualAlloc` and `VirtualFree` functions.

Each large allocation is stored in a linked list, so that the free
function can find it (to verify the pointer is valid).  This search
is currently not optimised.


MEDIUM OBJECT ALLOCATOR
-----------------------

The medium object allocator is a basic chunk-based allocator.
A "chunk" is a header containing the size of the chunk, a marker to
indicate whether the chunk is free of used, and links to the previous
and next chunks.  Each of these values are 32-bits, meaning each
chunk has an overhead of 16 bytes.  The free/used marker uses an
uncommon bit pattern so that erroneous frees and memory corruption
can be detected.

There will be multiple blocks using this system.  Each new block gets
progressively larger, ranging from a small size (4096 bytes) up to a
size limited by the 32-bit offsets: 2GB or (2^31) bytes.  These are
sorted into a list so that frees can find their corresponding block
as quickly as possible (via a binary search).


SMALL OBJECT ALLOCATOR
----------------------

The small object allocator packs objects tightly together into chunks
of 4096 bytes.  It categorizes objects into six different sizes, from
16 bytes to 256 bytes.  Each chunk contains a bitmap used to indicate
whether each object is free or used.  To make allocations even faster,
chunks which are completely full are kept in a separate "dead" list.

Like the medium allocator, the allocation of the underlying memory
blocks get progressively larger as the smaller ones become full.
These blocks are also stored in the same sorted list used by the
medium allocator, allowing quick lookup for the free operation.

